#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<HieuHT@VNOSS.ORG>
  Purpose: Checking Daemon Service Is Running or NOT 
  Created: 08/04/2015
"""
import socket
import sys
import optparse
import subprocess

# Define
EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRITICAL = 2
EXIT_UNKNOWN = 3

# Args
def build_parser():
    usage = """
    usage: %prog -s 
    -s: service name
    """    
    parser = optparse.OptionParser(usage= usage)
    parser.add_option("-s", "--service", dest="service", help="Service name.")
    return parser

def check(service):
    command = "service %s status" % service
    proc = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell= True)
    result = proc.communicate()[0]
    return result

def process(data):
    data = data.strip()
    message, status = None, None
    if 'unrecognized' in data:
        message, status = data, EXIT_CRITICAL
    elif 'stopped' in data or 'inactive' in data or 'ERROR' in data:
        message, status = data, EXIT_CRITICAL
    elif 'running' in data:
        message, status = data, EXIT_OK
    else:
        message, status = data, EXIT_UNKNOWN
    return message, status

def main():
    parser = build_parser()
    options, _args = parser.parse_args()
    if not options.service:
        parser.error("Service required")
    service = options.service
    data = check(service)
    message, status = process(data)
    print message
    sys.exit(status)
    

if __name__ == '__main__':
    main()