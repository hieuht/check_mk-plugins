#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<HieuHT@VNOSS.ORG>
  Purpose: Checking Port Is Open OR NOT
  Created: 08/04/2015
"""
import socket
import optparse
import sys

# Define
EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRITICAL = 2
EXIT_UNKNOWN = 3

def build_parser():
    usage = """
    usage: %prog -s MySQL -p 3306 -t 2000
    -i: IP, Default 127.0.0.1
    -s: Service Name, Default: TCP-PORT
    -p: Port
    -t: Default 2000
    """    
    parser = optparse.OptionParser(usage= usage)
    parser.add_option("-i", "--ip", dest="host", help="Host.", default="127.0.0.1")
    parser.add_option("-s", "--service", dest="service", help="Services Name.", default="TCP-PORT")
    parser.add_option("-p", "--port", dest="port", help="Port of redis", type="int")
    parser.add_option("-t", "--timeout", dest="timeout", help="Number of milliesconds to wait before timing out", type="int", default=2000)    
    return parser

def connect(service, host, port, timeout = None):
    message, status = None, None
    socket.setdefaulttimeout(timeout or None)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    
    response = s.connect_ex((host, port))
    if response == 0:
        message, status = "%s is opened on %s:%s" % (service, host, port), 0
    else:
        message, status = "%s is not opened on %s:%s" % (service, host, port), 2
    return message, status

def main():
    parser = build_parser()
    options, _args = parser.parse_args()
    if not options.port:
        parser.error("Port required")
    port = options.port
    host = options.host
    timeout = options.timeout
    service = options.service
    message, status = connect(service, host, port, timeout)
    print message
    sys.exit(status)
        

if __name__ == '__main__':
    main()