#!/bin/bash
#
# Author: Khanh Pham <khanh@mrpk.info>
# Desc  : Nagios plugin for count file in folder

OK=0
WARNING=1
CRITICAL=2

TODAY=$(date +"%Y-%m-%d")
TMP_FILE="/tmp/$(echo $1 | sed 's/\//_/g')"
TMP_TIMESTAMP="/tmp/last_check_time$(echo $1 | sed 's/\//_/g')"
#Time for wait to next check
WAIT=300
usage() {
  echo "Usage: $0 folder_for_count"
}

if [ ! -f $TMP_FILE ]; then
    touch $TMP_FILE
elif [ ! -f $TMP_TIMESTAMP ]; then
    touch $TMP_TIMESTAMP
fi

OLD_TIME_CHECK=$(cat $TMP_TIMESTAMP)
TIME_CHECK=$(date +%s)
let "t = ($TIME_CHECK - $OLD_TIME_CHECK)"
let "n = ($WAIT - $t)"

if [ -z "$1" ]; then
    usage
elif [ ! -d "$1" ]; then
    echo "Folder for count $1 not exist!!"
    exit $CRITICAL
elif [ "$t" -lt $WAIT ]; then
  echo "OK - Waiting "$n"s for next check"
  exit $OK
else
  echo "$TIME_CHECK" > "$TMP_TIMESTAMP"

  if [ ! -f "$TMP_FILE" ]; then
    echo "$FILE_COUNT" > "$TMP_FILE"
    echo "tmp file count not exist"
    exit $WARNING
  else
    if [ -d "$1/$TODAY" ]; then
      OLD_FILE_COUNT=$(cat $TMP_FILE)
      FILE_COUNT=$(ls $1/$TODAY | wc -l)
      echo "$FILE_COUNT" > "$TMP_FILE"
      if [ "$OLD_FILE_COUNT" -lt "$FILE_COUNT" ]; then
        echo "OK - Files counted in folder $1 now is $FILE_COUNT (old: $OLD_FILE_COUNT)"
        exit $OK
      elif [ "$OLD_FILE_COUNT" -eq "$FILE_COUNT" ]; then
        echo "CRITICAL - Not have any newer files in $1. Files counted is $OLD_FILE_COUNT"
        exit $CRITICAL
      else
        #echo "Error"
        echo "file counted: $FILE_COUNT, old file counted: $OLD_FILE_COUNT. t: $t"
        exit $CRITICAL
      fi
    fi
  fi
fi
