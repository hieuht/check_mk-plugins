#!/bin/bash
# Author: Hieu Ha Trung < hieuht@vnoss.org >
# Description: Check an file is empty or not
# Issues:
MYFILE=""
OK=0
CRITICAL=2
if [ ! $# -eq 0 ]; then
    MYFILE="$1"
    if [ -e $MYFILE ]; then
        if [[ ! -s $MYFILE ]] ; then
            echo "CRITICAL, $MYFILE is empty"
        else
            echo "OK, $content is current value"
            exit $OK
        fi
    else
        echo "CRITICAL, $MYFILE not found"
        exit $CRITICAL
    fi
fi