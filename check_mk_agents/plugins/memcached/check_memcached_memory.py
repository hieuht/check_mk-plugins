#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<HIEUHT@VNOSS.ORG>
  Purpose: Checking Memory Using In Each Memcached Port
  Created: 08/04/2015
"""
import socket
import sys
import optparse

# Define
EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRITICAL = 2
EXIT_UNKNOWN = 3

# Args
def build_parser():
    usage = """
    usage: %prog -s 192.168.12.20 -p 11211 -w 20 -c 30 -t 2000
    -s: Default 127.0.0.1
    -p: Default 11211
    -t: Default 2000
    -w: Unit is Megabyte (MB)
    -c: Unit is Megabyte (MB)
    """    
    parser = optparse.OptionParser(usage= usage)
    parser.add_option("-s", "--server", dest="host", help="Redis host to connect to.", default="127.0.0.1")
    parser.add_option("-p", "--port", dest="port", help="Port of redis", type="int", default=11211)
    parser.add_option("-w", "--warning", dest="warning", help="Memory utilization (in MB) that triggers a warning status", type="int")
    parser.add_option("-c", "--critial", dest="critial", help="Memory utilization (in MB) that triggers a critial status", type="int")
    parser.add_option("-t", "--timeout", dest="timeout", help="Number of milliesconds to wait before timing out and considering redis down", type="int", default=2000)    
    return parser

def connect(host, port, timeout = None):    
    socket.setdefaulttimeout(timeout or None)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, port))
        return get_info(s)
    except:
        print "Can not Open Socket to (%s:%s)" % (host, port)
        sys.exit(EXIT_CRITICAL)
        
def get_info(s):
    s.send('stats' + b'\r\n')
    buf = ''
    while '\r\n' not in buf: 
        buf += s.recv(2048)
    s.close()
    data = dict(x.replace('STAT ', '').split(' ', 1) for x in buf.split('\r\n') if ' ' in x)
    return data
    
def process_data(data, warning, critial):
    message, status = None, None
    memory_limit = int(data.get('limit_maxbytes')) / (1024 * 1024)
    memory = int(data.get("bytes")) / (1024 * 1024)
    free_memory = memory_limit - memory
    if free_memory < critial:
        message, status = "MEMCACHED memory usage %d/%dMB (free %dMB) (threshold %dMB)" % (memory, memory_limit, free_memory, critial), EXIT_CRITICAL
    elif free_memory < warning:
        message, status = "MEMCACHED memory usage %d/%dMB (free %dMB) (threshold %dMB)" % (memory, memory_limit, free_memory, warning), EXIT_WARN
    else:
        message, status = "REDIS memory usage %d/%dMB (free %dMB)" % (memory, memory_limit, free_memory), EXIT_OK
    return message, status

def main():    
    parser = build_parser()
    options, _args = parser.parse_args()
    if not options.warning:
        parser.error("Warning Level required")
    if not options.critial:
        parser.error("Critical Level required")

    host = options.host
    port = options.port
    timeout = options.timeout
    warning = options.warning
    critial = options.critial    
    data = connect(host, port)
    message, status = process_data(data, warning, critial)
    print message
    sys.exit(status)
    
if __name__ == '__main__':
    main()