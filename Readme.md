# Check-MK MRPE Plugins
* check_mk_agent: /usr/share/check-mk-agent/plugins/
* MRPE's Check_mk_agent: /etc/check-mk-agent/mrpe.cfg

**TCP PORT**

[X] REDIS 6379

[x] MEMCACHED 11211

[x] MYSQL 3306

** DAEMON SERVICES**

[x] Checking Service (mongodb|pure-ftpd|crond|php-fpm|mysql) Status

**Redis**

[x] Checking RAM Memory Is Used By Redis

**Memcached**

[x] Checking Memory Using In Each Memcached Port

**JAVA Services**

[ ] Checking JAVA Services Status

**PHP-FPM**

[ ] Checking Process Dead-lock

[ ] Counting PHP-FPM Process (Group by Pool Connection)

**NGINX**

[ ] Checking HTTP Status By HOSTNAME (VHOST)
