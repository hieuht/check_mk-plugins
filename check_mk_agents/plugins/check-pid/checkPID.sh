#!/bin/bash
# Author: Hieu Ha Trung < hieuht@vnoss.org >
# Description: Checking a proccess which is living or not?
PIDFILE=""
MYPID=""
OK=0
WARNING=1
CRITICAL=2
if [ ! $# -eq 0 ]
  then
    PIDFILE="$1"
    if [ -e "$PIDFILE" ]; then
        MYPID=`/bin/cat $PIDFILE`
        if [ -e "/proc/$PID" ]; then
            echo "OK, $PIDFILE is running"
            exit $OK
        else
            echo "CRITICAL, $PIDFILE was die"
            exit $CRITICAL
        fi
    else
        echo "CRITICAL, $PIDFILE not found"
        exit $CRITICAL
    fi
fi
