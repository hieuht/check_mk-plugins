servers="srv.6giosang.com srv1.chicago srv2.chicago srv.thanhdoanhaiphong srv4.singapore"
plugins=`find . -type f -name '*.py'`
MRPE_PLUGIN_PATH="/usr/share/check-mk-agent/plugins"
for server in $servers
do
    echo "Server $server"
    for plugin in $plugins
    do
        status_sync=`scp -P 15000 $plugin root@$server:$MRPE_PLUGIN_PATH/. && echo -n "DONE"`
        echo "\t SYNC: $plugin $status_sync"
    done
    status_chmod=`ssh -p 15000 root@$server chmod 755 $MRPE_PLUGIN_PATH/*.py && echo -n "CHMOD +X DONE"`
    echo "\t $status_chmod"
done