#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<HieuHT@VNOSS.ORG>
  Purpose: Checking Redis Memory
  Created: 08/04/2015
  Nagios status code (0=OK, 1=WARN, 2=CRIT, 3=UNKNOWN)
"""
import socket
import sys
import optparse
# Define
EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRITICAL = 2
EXIT_UNKNOWN = 3

# Args
def build_parser():
    usage = """
    usage: %prog -s 192.168.12.20 -p 6379 -w 20 -c 30 -a foobared -t 2000
    -s: Default 127.0.0.1
    -p: Default 6379
    -t: Default 2000
    -a: Default None
    -w: Unit is Megabyte (MB)
    -c: Unit is Megabyte (MB)
    """    
    parser = optparse.OptionParser(usage= usage)
    parser.add_option("-s", "--server", dest="host", help="Redis host to connect to.", default="127.0.0.1")
    parser.add_option("-p", "--port", dest="port", help="Port of redis", type="int", default=6379)
    parser.add_option("-w", "--warning", dest="warning", help="Memory utilization (in MB) that triggers a warning status", type="int")
    parser.add_option("-c", "--critial", dest="critial", help="Memory utilization (in MB) that triggers a critial status", type="int")
    parser.add_option("-a", "--auth", dest="auth", help="Redis Password.", default="None")
    parser.add_option("-t", "--timeout", dest="timeout", help="Number of milliesconds to wait before timing out and considering redis down", type="int", default=2000)    
    return parser

# Connect To Redis Server And Processing Auth (If has)
def connect(host, port, auth = None, timeout = 2000):
    socket.setdefaulttimeout(timeout or None)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:        
        s.connect((host, port))
        if None != auth:    
            s.send("auth " + auth + "\r\n")
            response = s.recv(1024)
            if("+OK" == response.strip()) or "-ERR Client sent AUTH" in response:
                data = get_info(s)
                return data
            else:
                print 'Wrong password to connect instance (%s:%s)' % (host, port)
                sys.exit(EXIT_CRITICAL)
        else:
            data = get_info(s)
            return data
    except:        
        print "Can not Open Socket to (%s:%s)" % (host, port)
        sys.exit(EXIT_CRITICAL)

# Get Redis's information
def get_info(s):
    s.send("info \r\n")
    buf = ''
    while '\r\n\r\n' not in buf: 
        buf += s.recv(1024)
    s.close()
    data = dict(x.split(':', 1) for x in buf.split('\r\n') if ':' in x)
    return data

# Make Warning, Critial, or OK
def process_data(data, warning, critial):
    message, status = None, None
    if None != data:
        # Memory Megabyte (MB)        
        memory = int(data.get("used_memory_rss") or info["used_memory"]) / (1024*1024) 
        if memory > critial:
            message, status = "REDIS memory usage is %dMB (threshold %dMB)" % (memory, critial), EXIT_CRITICAL
        elif memory > warning:
            message, status = "REDIS memory usage is %dMB (threshold %dMB)" % (memory, warning), EXIT_WARN
        else:
            message, status = "REDIS memory usage is %dMB" % memory, EXIT_OK
    return message, status

def main():
    parser = build_parser()
    options, _args = parser.parse_args()
    host = options.host
    port = options.port
    auth = options.auth
    timeout = options.timeout
    warning = options.warning
    critial = options.critial
    if not options.warning:
        parser.error("Warning Level required")
    if not options.critial:
        parser.error("Critical Level required")
    
    # Main
    data = connect(host, port, auth = auth, timeout = timeout)
    message, status = process_data(data, warning, critial)
    if None != message and None != status:
        print message
        sys.exit(status)
        
if __name__ == "__main__":
    main()